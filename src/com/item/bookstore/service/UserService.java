package com.item.bookstore.service;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.item.bookstore.mapper.UserMapper;
import com.item.bookstore.pojo.User;

@Service
public class UserService {

	@Autowired
	private UserMapper userMapper;
	

	public User findusername(String username){
		
		User user=userMapper.findusername(username);
		return user;
	}
	
	public void insertuser(User user){
		 
		
		userMapper.insertuser(user);
		
	}
	public boolean findpassword(String username,String password){
		
		String password1=userMapper.findpassword(username);
		
		if(password.equals(password1)) return true;
		else {
			return false;
		}
	}
	
	public Integer finduidbyusername(String username){
		Integer uid=userMapper.finduidbyusername(username);
		return uid;
	}
	
	public User finduserbyusername(String username){
		
		User user=userMapper.finduserbyusername(username);
		return user;
	}
	public void updateuser(User user){
		userMapper.updateuser(user);
	}
	public void deleteuser(Integer uid){
		userMapper.deleteuser(uid);
	}
	public Integer findlastuid(){
		Integer uid=userMapper.findlastuid();
		return uid;
	}
	public User finduserbyuid(Integer uid){
		User user=userMapper.finduserbyuid(uid);
				
		return user;
	}

	
	
}
