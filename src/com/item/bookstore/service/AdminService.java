package com.item.bookstore.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.item.bookstore.mapper.AdminMapper;
import com.item.bookstore.pojo.Admin;

@Service
public class AdminService {

	@Autowired
	private AdminMapper adminMapper;
	
	public boolean adminlogin(Admin admin){
		
		Admin admin2=adminMapper.adminlogin(admin);
		
		if(admin2!=null) {return true;}
		else   {return false;}
	}
	public void updateadminpw(String password){
		adminMapper.updateadminpw(password);
	}
}