package com.item.bookstore.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.item.bookstore.mapper.ProductMapper;
import com.item.bookstore.pojo.CartItem;
import com.item.bookstore.pojo.Product;

@Service
public class CartService {

	@Autowired
	private ProductMapper productMapper;
	
	public CartItem addcart(Integer pid,Integer num){
		
		Product product=productMapper.findproductbypid(pid);
		CartItem cartItem=new CartItem();
		cartItem.setProduct(product);
		cartItem.setPid(pid);
		cartItem.setNum(num);
		cartItem.setSubTotal(product.getShop_price()*num);
		
		return cartItem;
	}
}
