package com.item.bookstore.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.item.bookstore.mapper.CategoryMapper;
import com.item.bookstore.pojo.Category;

@Service
public class CategoryService {

	@Autowired
	private CategoryMapper categoryMapper;
	
	public List<Category> findcategory(){
		
		List<Category> categories=categoryMapper.findcategory();
		return categories;
	}
	
	public void deletecategory(Integer cid){
		categoryMapper.deletecategory(cid);
	}
	public void updatecategory(Category category){
		categoryMapper.updatecategory(category);
	}
	public void addcategory(String cname){
		categoryMapper.addcategory(cname);
	}
	public Integer findcidlast(){
		Integer cid=categoryMapper.findcidlast();
		return cid;
	}
	public Category selectcategorybycid(Integer cid){
		Category category=categoryMapper.selectcategorybycid(cid);
		
		return category;
	}
}