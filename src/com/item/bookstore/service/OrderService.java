package com.item.bookstore.service;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.omg.CORBA.PUBLIC_MEMBER;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.item.bookstore.mapper.OrderMapper;
import com.item.bookstore.mapper.UserMapper;
import com.item.bookstore.pojo.CartItem;
import com.item.bookstore.pojo.Category;
import com.item.bookstore.pojo.Order;
import com.item.bookstore.pojo.OrderItem;

@Service
public class OrderService {

	@Autowired
	public OrderMapper orderMapper;
	//��Ӷ���
	public void insertorders(Double total,Integer uid){
		
		Order order=new Order();
		order.setTotal(total);
		order.setState(0);
		order.setUid(uid);
		
		orderMapper.insertorders(order);
	}
	//���ظ���ӵ���ݵ�oid
	public Integer findoidlast(){
		Integer oid=orderMapper.findoidlast();
		return oid;
	}
	//��Ӷ�����
	public void insertorderitem(Integer oid,List<CartItem> cartItems){
		
		OrderItem orderItem=new OrderItem();
		for (CartItem cartItem : cartItems) {
			orderItem.setNum(cartItem.getNum());
			orderItem.setOid(oid);
			orderItem.setPid(cartItem.getPid());
			orderItem.setTotal(cartItem.getSubTotal());
			
			orderMapper.insertorderitem(orderItem);
			orderItem=new OrderItem();
		}
	}
	//ͨ��oid�������еĶ�����
	public List<OrderItem> findorderitembyoid(Integer oid){
		
		List<OrderItem> orderItems=new ArrayList<OrderItem>();
		orderItems=orderMapper.findorderitembyoid(oid);
		return orderItems;
	}
	//ͨ��uid�������еĶ���
	public List<Order> findordersbyuid(Integer uid){
		
		List<Order> orders=new ArrayList<Order>();
		orders=orderMapper.findordersbyuid(uid);
		return orders;
	}
	//ȷ�϶������޸Ķ�����ϢΪ�Ѹ���
	public void updateorders(Order order){
		
		orderMapper.updateorders(order);
	}
	
	public List<Order> findallorders(){
		
		List<Order> orders=orderMapper.findallorders();
		
		return orders;
	}
	public List<OrderItem> findallorderitem(){
		
		List<OrderItem> orderItems=orderMapper.findallorderitem();
		
		return orderItems;
		
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
	}

}
