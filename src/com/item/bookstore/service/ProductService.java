package com.item.bookstore.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.item.bookstore.mapper.ProductMapper;
import com.item.bookstore.pojo.Product;

@Service
public class ProductService {

	@Autowired
	private ProductMapper productMapper;
	
	public List<Product> findnewproduct(){
		List<Product> products=productMapper.findnewproduct();
		return products;
	}
	
	public List<Product> findhotproduct(){
		List<Product> products=productMapper.findhotproduct();
		return products;
	}
	
	public List<Product> findproductbycid(Integer cid){
		List<Product> products=productMapper.findproductbycid(cid);
		return products;
	}
	
	public Product findproductbypid(Integer pid){
		Product product=productMapper.findproductbypid(pid);
		return product;
	}
	
	public List<Product> findallproduct(){
		
		List<Product> products=productMapper.findallproduct();
		
		return products;
	}
	public void addproduct(Product product){
		
		productMapper.addproduct(product);
		
	}
	public Integer findpidlast(){
		Integer pid=productMapper.findpidlast();
		return pid;
	}
	public Product findproductbypname(String pname){
		
		Product product=productMapper.findproductbypname(pname);
		
		return product;
	}
	public Product findproductbypnameandcid(Product product){
		
		Product product2=productMapper.findproductbypnameandcid(product);
		
		return product2;
	}
	public void updateproduct(Product product){
		
		productMapper.updateproduct(product);
	}
	public void deleteproduct(Integer pid){
		productMapper.deleteproduct(pid);
	}
}
