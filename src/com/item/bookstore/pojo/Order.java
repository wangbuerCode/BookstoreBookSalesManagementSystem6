package com.item.bookstore.pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Order {
	
	private Integer oid;  //订单ID
	private String orderTime;//下单时间
	private int state;//状态 0:未付款 1已付款
	private double total;//总计
	private String address;  //收货人地址
	private String name;  //收货人姓名
	private String telephone;  //收货人电话
	private Integer uid;
	
	
	//当前订单下有多少订单项
	private List<OrderItem> list=new ArrayList<OrderItem>();

	
	@Override
	public String toString() {
		return "Order [oid=" + oid + ", orderTime=" + orderTime + ", state="
				+ state + ", total=" + total + ", address=" + address
				+ ", name=" + name + ", telephone=" + telephone + ", uid="
				+ uid + ", list=" + list + "]";
	}

	public Integer getOid() {
		return oid;
	}

	public void setOid(Integer oid) {
		this.oid = oid;
	}

	public String getOrderTime() {
		return orderTime;
	}

	public void setOrderTime(String orderTime) {
		this.orderTime = orderTime;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public Integer getUid() {
		return uid;
	}

	public void setUid(Integer uid) {
		this.uid = uid;
	}

	public List<OrderItem> getList() {
		return list;
	}

	public void setList(List<OrderItem> list) {
		this.list = list;
	}

	

}
