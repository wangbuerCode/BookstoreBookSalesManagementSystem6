package com.item.bookstore.pojo;

import javax.management.loading.PrivateClassLoader;

public class OrderItem {
	
	private Integer itemid;//id
	private int num;//数量
	private double total;//小计
	private Integer pid;
	private Integer oid;
	private Product product;

	public Integer getItemid() {
		return itemid;
	}

	public void setItemid(Integer itemid) {
		this.itemid = itemid;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public Integer getPid() {
		return pid;
	}

	public void setPid(Integer pid) {
		this.pid = pid;
	}

	public Integer getOid() {
		return oid;
	}

	public void setOid(Integer oid) {
		this.oid = oid;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	@Override
	public String toString() {
		return "OrderItem [itemid=" + itemid + ", num=" + num + ", total="
				+ total + ", pid=" + pid + ", oid=" + oid + ", product="
				+ product + "]";
	}
	
	
}
