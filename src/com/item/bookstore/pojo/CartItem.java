package com.item.bookstore.pojo;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class CartItem {
    private Product product;
	private int num; //数量
	private double subTotal; //小计
	private Integer pid;
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public double getSubTotal() {
		return subTotal;
	}
	public void setSubTotal(double subTotal) {
		this.subTotal = subTotal;
	}
	public Integer getPid() {
		return pid;
	}
	public void setPid(Integer pid) {
		this.pid = pid;
	}
	@Override
	public String toString() {
		return "CartItem [product=" + product + ", num=" + num + ", subTotal="
				+ subTotal + ", pid=" + pid + "]";
	}
	
	
}
