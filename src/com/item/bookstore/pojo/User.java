package com.item.bookstore.pojo;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class User {
	private Integer uid;
	private String username;
	private String password;
	private String name;
    private String address;
	private String telephone;
	 @DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date birthday;
	private String sex;


	public Integer getUid() {
		return uid;
	}


	public void setUid(Integer uid) {
		this.uid = uid;
	}


	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getPassword() {
		return password;
	}






	public void setPassword(String password) {
		this.password = password;
	}






	public String getName() {
		return name;
	}






	public void setName(String name) {
		this.name = name;
	}






	public String getTelephone() {
		return telephone;
	}






	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}






	public Date getBirthday() {
		return birthday;
	}




	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}




	public String getSex() {
		return sex;
	}





	public void setSex(String sex) {
		this.sex = sex;
	}








	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}






	@Override
	public String toString() {
		return "User [uid=" + uid + ", username=" + username + ", password="
				+ password + ", name=" + name + ", address=" + address
				+ ", telephone=" + telephone + ", birthday=" + birthday
				+ ", sex=" + sex + "]";
	}

	
}
