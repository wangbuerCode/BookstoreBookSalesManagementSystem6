package com.item.bookstore.controller;

import java.util.ArrayList;
import java.util.List;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.item.bookstore.mapper.AdminMapper;
import com.item.bookstore.mapper.UserMapper;
import com.item.bookstore.pojo.Admin;
import com.item.bookstore.pojo.Category;
import com.item.bookstore.pojo.Product;
import com.item.bookstore.pojo.User;
import com.item.bookstore.service.AdminService;
import com.item.bookstore.service.CategoryService;
import com.item.bookstore.service.ProductService;
import com.item.bookstore.service.UserService;

@Controller
public class AdminController {

	@Autowired
	private AdminMapper adminMapper;
	@Autowired
	private AdminService adminService;
	
	@Autowired
	private UserMapper userMapper;
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private CategoryService categoryService;
	
	@Autowired
	private UserService userService;
	//����Ա��¼
	@RequestMapping(value="/adminlogin.action")
	public String adminlogin(String name,String password,HttpServletRequest request){
		System.out.println(name+"====="+password);
		Admin admin=new Admin();
		admin.setName(name);
		admin.setPassword(password);
		
		if(adminService.adminlogin(admin)==false)
		{
			request.setAttribute("asg", "请重新登录");
			return "/admin/adminlogin.jsp";
		}
		else {
			return "/selectallproduct.action";
		}
	}
	//��¼����ת����Ʒ����ҳ��
	@RequestMapping(value="/selectallproduct.action")
	public String selectallproduct(HttpSession session){
		List<Category> categories=categoryService.findcategory();
		List<Product> products=productService.findallproduct();
		
		session.setAttribute("selectproduct", products);
		session.setAttribute("selectcategory", categories);
		return "/admin/index.jsp";
	}
	//��ת�������Ʒҳ��
	@RequestMapping(value="/openaddproduct.action")
	public String openaddproduct(HttpSession session){
		
		List<Category> categories=categoryService.findcategory();
		session.setAttribute("selectcategory", categories);
		return "/admin/addproduct.jsp";
	}
	//ʵ�������Ʒ����
	@RequestMapping(value="/addproduct.action")
	public String addproduct(Product product,Model model,HttpServletRequest request,HttpSession session){
		 
		String is_hot=request.getParameter("is_hot");
		int hot=0;
		if(is_hot=="0")
		{hot=0;}
		else {
			hot=1;
		}
		String cid=request.getParameter("cid");
		Integer cidd=Integer.valueOf(cid);
		product.setCid(cidd);
		product.setIs_hot(hot);
		
		productService.addproduct(product);
		Integer pid=productService.findpidlast();
        Product product2=productService.findproductbypid(pid);
		List<Product> products=new ArrayList<Product>();
		products.add(product2);
		session.setAttribute("selectproduct", products);
		
		return "/admin/index.jsp";
	}
	//��ת���޸���Ʒ��Ϣҳ��
	@RequestMapping(value="/openupdateproduct.action")
	public String openupdateproduct(Integer pid,Model model){
		
		Product product=productService.findproductbypid(pid);
		model.addAttribute("updateproduct", product);
		return "/admin/updateproduct.jsp";
	}
	//ʵ��ɾ����Ʒ���ܣ����ض�����Ʒ����ҳ��
	@RequestMapping(value="/deleteproduct.action")
	public String deleteproduct(@RequestParam("pid")Integer pid){
		
		productService.deleteproduct(pid);
		
		return "redirect:/selectallproduct.action";
	}
	//ʵ���޸���Ʒ����
	@RequestMapping(value="/updateproduct.action")
	public String updateproduct(Product product,Model model){
		System.out.println("======"+product.getPid());
		System.out.println(product.toString());
		productService.updateproduct(product);
		Product product2=productService.findproductbypid(product.getPid());
		List<Product> products=new ArrayList<Product>();
		products.add(product2);
		
        model.addAttribute("selectproduct", products);
		
		return "/admin/index.jsp";
	}
	
	
	//ʵ����Ʒ��ѯ����
	@RequestMapping(value="/selectproduct.action")
	public String selectproduct(String pname,Integer cid,Model model,HttpSession session){
		
		List<Product> products=new ArrayList<Product>();
		session.removeAttribute("selectproduct");
		if(pname==null||pname=="")
		{
			if(cid!=null)
			{
				products=productService.findproductbycid(cid);
				model.addAttribute("selectproduct", products);
			}
		}
		else {
			if(cid==null)
			{
				Product product=productService.findproductbypname(pname);
				products.add(product);
				model.addAttribute("selectproduct", products);
			}
			else {
				Product product=new Product();
				product.setCid(cid);
				product.setPname(pname);
				Product product2=productService.findproductbypnameandcid(product);
				products.add(product2);
				model.addAttribute("selectproduct", products);
			}
		}
		return "/admin/index.jsp";
	}
	//�û�����ҳ��
	@RequestMapping(value="/usermanage.action")
	public String usermanage(HttpSession session){
		List<User> users=userMapper.findalluser();
		session.setAttribute("findalluser", users);
		return "/admin/usermanage.jsp";
	}
	//ʵ������û�����
	@RequestMapping(value="/adduser.action")
	public String addcategory(User user,Model model){
		userService.insertuser(user);
		Integer uid=userService.findlastuid();
		
		user.setUid(uid);
		List<User> users=new ArrayList<User>();
		users.add(user);
		model.addAttribute("findalluser", users);
		 return "/admin/usermanage.jsp";
	}
	//��ת���޸��û�ҳ��
	@RequestMapping(value="/openupdateuser.action")
	public String openupdateuser(Integer uid,Model model){
		User user=userMapper.finduserbyuid(uid);
		
		model.addAttribute("updateuser",user);
		 return "/admin/updateuser.jsp";
		
	}
	//ʵ���޸��û�����
	@RequestMapping(value="/updateuser.action")
	public String updateuser(Integer uid,User user,Model model){
		
		user.setUid(uid);
		
		userService.updateuser(user);
		List<User> users=new ArrayList<User>();
		users.add(user);
		model.addAttribute("findalluser", users);
		return "/admin/usermanage.jsp";
	}
	//ɾ���û�
	@RequestMapping(value="/deleteuser.action")
	public String deleteuser(Integer uid){
		
		userService.deleteuser(uid);
		
		return "redirect:/usermanage.action";
	}
	//��ѯ�û���Ϣ
	@RequestMapping(value="/selectuser.action")
	public String selectuser(Integer uid,Model model){
		
		User user=userMapper.finduserbyuid(uid);
		
		List<User> users=new ArrayList<User>();
		users.add(user);
		model.addAttribute("findalluser", users);
		return "/admin/usermanage.jsp";
	}
	
	//��Ʒ����ҳ��
		@RequestMapping(value="/categorymanage.action")
		public String categorymanage(HttpSession session){
			List<Category> categories=categoryService.findcategory();
			session.setAttribute("selectcategory", categories);
			return "/admin/categorymanage.jsp";
		}
		//ʵ�������Ʒ���๦��
		@RequestMapping(value="/addcategory.action")
		public String addcategory(String cname,Model model){
			categoryService.addcategory(cname);
			Integer cid=categoryService.findcidlast();
			Category category=new Category();
			category.setCid(cid);
			category.setCname(cname);
			
			List<Category> categories=new ArrayList<Category>();
			categories.add(category);
			model.addAttribute("selectcategory", categories);
			 return "/admin/categorymanage.jsp";
		}
		//��ת���޸���Ʒ����ҳ��
		@RequestMapping(value="/openupdatecategory.action")
		public String openupdatecategory(Integer cid,String cname,Model model){
			Category category=new Category();
			category.setCid(cid);
			category.setCname(cname);
			
			model.addAttribute("cid", cid);
			model.addAttribute("cname",cname);
			 return "/admin/updatecategory.jsp";
			
		}
		//ʵ���޸���Ʒ���๦��
		@RequestMapping(value="/updatecategory.action")
		public String updatecategory(Integer cid,String cname,Model model){
			
			Category category=new Category();
			category.setCid(cid);
			category.setCname(cname);
			
			categoryService.updatecategory(category);
			List<Category> categories=new ArrayList<Category>();
			categories.add(category);
			model.addAttribute("selectcategory", categories);
			return "/admin/categorymanage.jsp";
		}
		//ɾ����Ʒ����
		@RequestMapping(value="/deletecategory")
		public String deletecategory(Integer cid){
			
			categoryService.deletecategory(cid);
			
			return "redirect:/categorymanage.action";
		}
		//��ѯ��Ʒ����
		@RequestMapping(value="/selectcategory.action")
		public String selectcategory(Integer cid,Model model){
			
			Category category=categoryService.selectcategorybycid(cid);
			
			List<Category> categories=new ArrayList<Category>();
			categories.add(category);
			model.addAttribute("selectcategory", categories);
			return "/admin/categorymanage.jsp";
		}
		//�޸Ĺ���Ա����
		@RequestMapping(value="/openupdatepw.action")
		public String adminpassword(String password,Model model){
			String password1=adminMapper.findadminpw();
			model.addAttribute("adminpw", password1);
			
			return "/admin/updateadminpw.jsp";
		}
		@RequestMapping(value="/updatepw.action")
		public String updateadminpw(String password,Model model){
			if(password==null||password=="")
			{
				model.addAttribute("asp", "信息为空");
				String password1=adminMapper.findadminpw();
				model.addAttribute("adminpw", password1);
				return "/admin/updateadminpw.jsp";
			}
			if(password.equals(adminMapper.findadminpw()))
			{
				model.addAttribute("asp", "请重新输入密码，勿与上次密码相同");
				String password1=adminMapper.findadminpw();
				model.addAttribute("adminpw", password1);
				return "/admin/updateadminpw.jsp";
			}
			else {
				adminService.updateadminpw(password);
				return "redirect:/selectallproduct.action";
			}
		}
		@RequestMapping(value="/adminlogout.action")
		public String adminlogout(){
			return "/admin/adminlogin.jsp";
		}
}
