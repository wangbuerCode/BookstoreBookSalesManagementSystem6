package com.item.bookstore.controller;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.item.bookstore.pojo.Category;
import com.item.bookstore.pojo.Product;
import com.item.bookstore.service.CategoryService;
import com.item.bookstore.service.ProductService;

@Controller
public class ProductController {

	@Autowired
	private CategoryService categoryService;
	
	@Autowired
	private ProductService productService;
	
	
	@RequestMapping(value="/index.action")
	public String findallproduct(Model model,HttpSession session){
		
		List<Category> categories=categoryService.findcategory();
		session.setAttribute("allCats", categories);
		
		List<Product> newproduct=productService.findnewproduct();
		List<Product> hotproduct=productService.findhotproduct();
		
		model.addAttribute("findnewproduct", newproduct);
		model.addAttribute("findhotproduct", hotproduct);	
		return "/jsp/index.jsp";
	}
	
	@RequestMapping(value="/category.action")
	public String findproductbycid(Integer cid,Model model){
		
		List<Product> products=productService.findproductbycid(cid);
		model.addAttribute("product_list", products);
		
		return "/jsp/product_list.jsp";
	}
	
	@RequestMapping(value="/findproduct.action")
	public String findproductbypid(Integer pid,Model model){
		Product product=productService.findproductbypid(pid);
		model.addAttribute("product_info", product);
		
		return "/jsp/product_info.jsp";
	}
}
