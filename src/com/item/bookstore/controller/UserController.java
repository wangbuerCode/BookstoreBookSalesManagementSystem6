package com.item.bookstore.controller;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.item.bookstore.pojo.User;
import com.item.bookstore.service.UserService;

@Controller
public class UserController {

	@Autowired
	private UserService userService;
	
	@RequestMapping(value="/register.action")
	public String register(User user,Model model){
		
		if(user.getUsername()==""||user.getPassword()=="")
	{model.addAttribute("msg1", "请重新输入");
	return "/jsp/register.jsp";
	}
	
		else {
			
			User user1=userService.findusername(user.getUsername());
			if(user1==null)
			{userService.insertuser(user);
				
			model.addAttribute("msg", "注册成功");
			return "/jsp/info.jsp";}
			
			else {
				model.addAttribute("msg2", "该账户已被注册");
				return "/jsp/register.jsp";}
	}
		
	}
	
	@RequestMapping(value="/login.action")
	public String login(String username,String password,Model model,HttpSession session){
		if(userService.findusername(username)==null)
		{
			model.addAttribute("msg3", "信息错误，请重新登录");
			return "/jsp/login.jsp";
		}
		else {
			if(userService.findpassword(username, password)==true)
			{
				session.setAttribute("loginUser", username);
				return "redirect:/index.action";
			}
			else {
				model.addAttribute("msg3", "账号或者密码错误");
				return "/jsp/login.jsp";
			}
		}
	}
	
	@RequestMapping(value="logout.action")
	public String logout(HttpSession session){
		
		session.removeAttribute("loginUser");
		return "redirect:/index.action";
	}
	
}
