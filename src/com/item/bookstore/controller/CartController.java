package com.item.bookstore.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.item.bookstore.mapper.ProductMapper;
import com.item.bookstore.pojo.CartItem;
import com.item.bookstore.pojo.Product;
import com.item.bookstore.service.CartService;

@Controller
public class CartController {
	
	@Autowired
	private CartService cartService;
	
	@RequestMapping(value="/opencart.action")
	public String opencart(HttpSession session,Model model){
		
		
		return "/jsp/cart.jsp";
	}

	@RequestMapping(value="/addcart.action")
	public String addcart(Integer pid,Integer num,HttpSession session,Model model){
		
		if(session.getAttribute("loginUser")==null)
		{
			model.addAttribute("msg", "请登录");
			return "/jsp/login.jsp";
		}
		
		CartItem cartItem=cartService.addcart(pid, num);
		System.out.println(cartItem);
		
		if(session.getAttribute("carts")!=null)
		{
			List<CartItem> cartItems=(List<CartItem>) session.getAttribute("carts");
			boolean c=false;
			 List<CartItem> cartItemslist=new ArrayList<CartItem>();
			 List<Product> productlist=new ArrayList<Product>();
			for (CartItem cartItem2 : cartItems) {
				if(cartItem2.getProduct().getPid()==cartItem.getProduct().getPid())
				{
					cartItem2.setNum(cartItem2.getNum()+cartItem.getNum());
					cartItem2.setSubTotal(cartItem2.getSubTotal()+cartItem.getSubTotal());
					c=true;
				}
				cartItemslist.add(cartItem2);
				productlist.add(cartItem2.getProduct());
			}
			if(c==true){
				cartItems.clear();
				cartItems.addAll(cartItemslist);
				session.setAttribute("carts", cartItems);
				session.setAttribute("productlists", productlist);
			}
			else {
				cartItems.add(cartItem);
				session.setAttribute("carts", cartItems);
				productlist.add(cartItem.getProduct());
				session.setAttribute("productlists", productlist);
			}
			Double total=(Double) session.getAttribute("total")+cartItem.getSubTotal();
			session.setAttribute("total", total);
		}
		else {
			List<CartItem> cartItems=new ArrayList<CartItem>();
			cartItems.add(cartItem);
			List<Product> productlist=new ArrayList<Product>();
			productlist.add(cartItem.getProduct());
			Double total=cartItem.getSubTotal();
			session.setAttribute("carts", cartItems);
			session.setAttribute("productlists", productlist);
			session.setAttribute("total", total);
		}

		return "/jsp/cart.jsp";
	}
	//ɾ������
	@RequestMapping(value="/deletecart.action")
	public String deletecart(Integer pid,HttpSession session){
		
		List<CartItem> cartItems=(List<CartItem>) session.getAttribute("carts");
		List<Product> productlist=(List<Product>) session.getAttribute("productlists");
		Double total=(Double) session.getAttribute("total");
		for (CartItem cartItem : cartItems) {
			if(cartItem.getProduct().getPid()==pid)
			{
				cartItems.remove(cartItem);
				productlist.remove(cartItem.getProduct());
				total=total-cartItem.getSubTotal();
				break;
			}
		}
		
		session.setAttribute("carts", cartItems);
		session.setAttribute("productlists", productlist);
		session.setAttribute("total", total);
		return "/jsp/cart.jsp";
	}
	//����ﳵ
	@RequestMapping(value="/clearcart.action")
	public String clearcart(HttpSession session){
		
		session.removeAttribute("carts");
		return "/jsp/cart.jsp";
	}
    
}

