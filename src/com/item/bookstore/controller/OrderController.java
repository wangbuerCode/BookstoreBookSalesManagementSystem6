package com.item.bookstore.controller;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;


import javax.servlet.http.HttpSession;

import org.omg.CORBA.PUBLIC_MEMBER;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.item.bookstore.pojo.CartItem;
import com.item.bookstore.pojo.Category;
import com.item.bookstore.pojo.Order;
import com.item.bookstore.pojo.OrderItem;
import com.item.bookstore.pojo.Product;
import com.item.bookstore.pojo.User;
import com.item.bookstore.service.OrderService;
import com.item.bookstore.service.ProductService;
import com.item.bookstore.service.UserService;

@Controller
public class OrderController {

	@Autowired
	private OrderService orderservice;
	@Autowired
	private UserService userService;
	@Autowired
	private ProductService productService;
	
	//�ύ����
	@RequestMapping(value="/submitorder.action")
	public String submitorder(HttpSession session,Model model) throws Exception {
		
		
		Double total=(Double) session.getAttribute("total");
		String username=(String) session.getAttribute("loginUser");
		Integer uid=userService.finduidbyusername(username);
		
		orderservice.insertorders(total, uid);
		Integer oid=orderservice.findoidlast();
		List<CartItem> cartItems=(List<CartItem>) session.getAttribute("carts");
		
		orderservice.insertorderitem(oid, cartItems);
		session.removeAttribute("carts");
		List<OrderItem> orderItems=orderservice.findorderitembyoid(oid);
		List<OrderItem> orderItems2=new ArrayList<OrderItem>();
		for (OrderItem orderItem : orderItems) {
			Product product=productService.findproductbypid(orderItem.getPid());
			orderItem.setProduct(product);
			orderItems2.add(orderItem);
		}
		session.setAttribute("orderItemlist", orderItems2);
		session.setAttribute("total", total);
		session.setAttribute("orderid", oid);
		
		User user=userService.finduserbyusername(username);
		model.addAttribute("user_list", user);
		return "/jsp/order_info.jsp";
	}
	
	//ȷ�϶���
	@RequestMapping(value="/payOrder.action")
	public String payOrder(HttpSession session,String address,String name,String telephone,Model model){
		
		Integer oid=(Integer) session.getAttribute("orderid");
		Order order=new Order();
		order.setOid(oid);
		order.setAddress(address);
		order.setName(name);
		order.setTelephone(telephone);
		orderservice.updateorders(order);
		
		model.addAttribute("msg", "成功！");
		return "/jsp/info.jsp";
	}
	
	//�鿴�ҵĶ���
	@RequestMapping(value="/openorder.action")
	public String openorder(HttpSession session){
		//��ȡ��ǰ���û���
		String username=(String) session.getAttribute("loginUser");
		Integer uid=userService.finduidbyusername(username);
		//������û����ж���
		List<Order> orders=orderservice.findordersbyuid(uid);
		//�Ѹö��������ж������װ��list��orders
		List<OrderItem> orderItems=new ArrayList<OrderItem>();
		List<Order> orders2=new ArrayList<Order>();
		for (Order order : orders) {
			orderItems=orderservice.findorderitembyoid(order.getOid());
			List<OrderItem> orderItems2=new ArrayList<OrderItem>();
			for (OrderItem orderItem : orderItems) {
				
				Product product=productService.findproductbypid(orderItem.getPid());
				orderItem.setProduct(product);
				orderItems2.add(orderItem);
			}
			order.setList(orderItems2);
			orders2.add(order);
			orderItems=new ArrayList<OrderItem>();
		}
		
		session.setAttribute("order_list", orders2);
		return "/jsp/order_list.jsp";
	}
	//δ�������Ʒ
	@RequestMapping(value="/payorder2.action")
	public String payorder2(HttpSession session,Model model,Integer oid,Double total){
		
		List<OrderItem> orderItems=orderservice.findorderitembyoid(oid);
		
		List<OrderItem> orderItems2=new ArrayList<OrderItem>();
		for (OrderItem orderItem : orderItems) {
			Product product=productService.findproductbypid(orderItem.getPid());
			orderItem.setProduct(product);
			orderItems2.add(orderItem);
		}
		session.setAttribute("orderItemlist", orderItems2);
		session.setAttribute("total", total);
		session.setAttribute("orderid", oid);
		
		String username=(String) session.getAttribute("loginUser");
		User user=userService.finduserbyusername(username);
		model.addAttribute("user_list", user);
		return "/jsp/order_info.jsp";
	}
}
