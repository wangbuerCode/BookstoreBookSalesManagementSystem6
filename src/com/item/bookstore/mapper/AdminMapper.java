package com.item.bookstore.mapper;

import com.item.bookstore.pojo.Admin;

public interface AdminMapper {

	public Admin adminlogin(Admin admin);
	
	public void updateadminpw(String password);
	
	public String findadminpw();
}