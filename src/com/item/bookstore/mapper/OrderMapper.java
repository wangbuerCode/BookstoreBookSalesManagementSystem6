package com.item.bookstore.mapper;
import java.util.List;

import com.item.bookstore.pojo.Order;
import com.item.bookstore.pojo.OrderItem;

public interface OrderMapper {

	public void insertorders(Order order);
	
	public Integer findoidlast();
	
	public void insertorderitem(OrderItem orderItem);
	
	public List<OrderItem> findorderitembyoid(Integer oid);
	
	public List<Order> findordersbyuid(Integer uid);
	
	public void updateorders(Order order);
	
	public List<Order> findallorders();
	
	public List<OrderItem> findallorderitem();
}
