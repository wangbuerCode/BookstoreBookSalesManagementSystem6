package com.item.bookstore.mapper;

import java.util.List;

import com.item.bookstore.pojo.Category;
import com.item.bookstore.pojo.User;


public interface UserMapper {

	public User findusername(String username);
	
	public void insertuser(User user);
	
	public String findpassword(String username);
	
	public Integer finduidbyusername(String username);
	
	public User finduserbyusername(String username);
	
	public void updateuser(User user);
	
	public void deleteuser(Integer uid);
	
	public Integer findlastuid();
	
	public User finduserbyuid(Integer uid);
	
	public List<User> findalluser();
}
