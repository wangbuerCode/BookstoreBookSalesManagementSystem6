package com.item.bookstore.mapper;
import java.util.List;

import com.item.bookstore.pojo.Product;

public interface ProductMapper {

	public List<Product> findnewproduct();
	
	public List<Product> findhotproduct();
	
	public List<Product> findproductbycid(Integer cid);
	
	public Product findproductbypid(Integer pid);
	
	public List<Product> findallproduct();
	
	public void addproduct(Product product);
	
	public Integer findpidlast();
	
	public Product findproductbypname(String pname);
	
	public Product findproductbypnameandcid(Product product);
	
	public void updateproduct(Product product);
	
	public void deleteproduct(Integer pid);
}
