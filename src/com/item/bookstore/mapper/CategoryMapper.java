package com.item.bookstore.mapper;
import java.util.List;

import com.item.bookstore.pojo.Category;

public interface CategoryMapper {

	public List<Category> findcategory();
	
	public void deletecategory(Integer cid);
	
	public void updatecategory(Category category);
	
	public Integer addcategory(String cname);
	
	public Integer findcidlast();
	
	public Category selectcategorybycid(Integer cid);
}
